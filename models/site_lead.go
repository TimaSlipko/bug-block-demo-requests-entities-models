package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
	"gorm.io/datatypes"
)

type SiteLead struct {
	ID    uint           `json:"id"`
	Email string         `json:"email"`
	Data  datatypes.JSON `json:"data"`
}

func GetSiteLead(siteLead entity.SiteLead) SiteLead {
	siteLeadModel := SiteLead{}
	siteLeadModel.ID = siteLead.ID
	siteLeadModel.Email = siteLead.Email
	siteLeadModel.Data = siteLead.Data
	return siteLeadModel
}

func GetSiteLeadArray(array []entity.SiteLead) []SiteLead {
	formatted := []SiteLead{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetSiteLead(arrayEl))
	}

	return formatted
}
