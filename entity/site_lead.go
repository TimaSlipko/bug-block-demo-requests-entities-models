package entity

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type SiteLead struct {
	gorm.Model
	ID    uint
	Email string `gorm:"type:text"`
	Data  datatypes.JSON
}
